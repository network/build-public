# gitlab-registry.cern.ch/network/build-public:grails
#
# Docker image based on CERN JDK 8 that configures Grails to build IT-CS application.
#
# VERSION  1.3
# AUTHOR   mkhelif
#
FROM gitlab-registry.cern.ch/network/build-public:jdk
MAINTAINER Marwan Khelif <marwan.khelif@cern.ch>

ENV GRAILS_VERSION 2.4.5

# Install Grails
RUN curl -fsSL https://github.com/grails/grails-core/releases/download/v$GRAILS_VERSION/grails-$GRAILS_VERSION.zip -o /usr/lib/jvm/grails-$GRAILS_VERSION.zip && \
    unzip /usr/lib/jvm/grails-$GRAILS_VERSION.zip -d /usr/lib/jvm && \
    rm -rf /usr/lib/jvm/grails-$GRAILS_VERSION.zip && \
    ln -s /usr/lib/jvm/grails-$GRAILS_VERSION /usr/lib/jvm/grails

# Setup Grails path
ENV GRAILS_HOME /usr/lib/jvm/grails
ENV PATH $PATH:$GRAILS_HOME/bin

# Install CodeNarc
RUN mkdir -p /opt/linters/codenarc && \
    curl -fsSL http://central.maven.org/maven2/org/codenarc/CodeNarc/0.27.0/CodeNarc-0.27.0.jar -o /opt/linters/codenarc/CodeNarc-0.27.0.jar && \
    curl -fsSL http://central.maven.org/maven2/log4j/log4j/1.2.14/log4j-1.2.14.jar -o /opt/linters/codenarc/log4j-1.2.14.jar && \
    curl -fsSL http://central.maven.org/maven2/org/codehaus/groovy/groovy-all/2.4.10/groovy-all-2.4.10.jar -o /opt/linters/codenarc/groovy-all-2.4.10.jar && \
    curl -fsSL http://central.maven.org/maven2/org/gmetrics/GMetrics/0.7/GMetrics-0.7.jar -o /opt/linters/codenarc/GMetrics-0.7.jar && \
    apt-get update && apt-get install -y bc

ADD codenarc.xml /opt/linters/codenarc/codenarc.xml
