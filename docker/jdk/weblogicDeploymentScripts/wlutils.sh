#
# Weblogic utilities functions.
#
# Author: npapapet


#
# arg[1] The weblogic domain
# arg[2] The variable where the adminServer name will be returned
#
function getAdminServer() {
    #Check if the cluster-name given is compliant with the convention
    if [[ $1 =~ ^(wls)_(csdb[0-9]{1,4})_([a-z]{1,})$ ]]; then
      eval $2="${BASH_REMATCH[1]}-${BASH_REMATCH[2]}-${BASH_REMATCH[3]}-admin.cern.ch"
    else
      echo "The server name is not compliant with the convention, please check if you misspelled something"
      exit 1
    fi
}

#
# NOTE: This function only returns the very first Apache server, this will change once we use DNS load-balancing.
#
# arg[1] The weblogic domain
# arg[2] The variable where the first apacheServer name will be returned
#
function getApacheServer () {
    weblogicDomain=$1

    #Check if the WebLogic domain given is compliant with the convention
    if [[ "$weblogicDomain" =~ ^wls_csdb[0-9]{1,4}_[a-z]{1,}$ ]]; then

        if [ "$weblogicDomain" == "wls_csdb04_prod" ]; then
            eval "$2='itrac5301-ip1'"
            return 0
        elif [ "$weblogicDomain" == "wls_csdb14_prod" ]; then
            eval "$2='db-ga564'"
            return 0
        elif [ "$weblogicDomain" == "wls_csdb03_prod" ]; then
            eval "$2='db-ga534'"
            return 0
        elif [ "$weblogicDomain" == "wls_csdb13_prod" ]; then
            eval "$2='db-ga563'"
            return 0
        fi

        echo "$0: no known Apache server for this WebLogic domain!"
        exit 1
    else
      echo "The WebLogic domain is not compliant with the convention, please check if you misspelled something"
      exit 1
    fi
}


# Retrieve on what WebLogic domain an application is running
#
# arg[1] Application name (csdb, landb, guestwifi)
# arg[2] The variable where the Weblogic domain will be returned.
#
function getCurrentApplicationDomain() {
    # remove (if it exists)and add ".cern.ch"
    applicationName=${1%.cern.ch}.cern.ch
    #echo "${applicationName}"

    # gets the IP address of the apache server where our application is running
    apacheServerIp=$(getIPByDNSName "$applicationName")
    #echo "${apacheServerIp}"

    # gets the host name for the ip address
    apacheServerHost=$(getHostNameByIP "$apacheServerIp")
    #echo "${apacheServerHost}"

    # connect to an default admin server to get the apache domain
    apacheDomain=$(ssh "landb@wls-csdb04-prod-admin.cern.ch" "vegas-cli interfacetab sc_interface_domain=$applicationName sc_entity sc_listen_domain | grep $apacheServerHost | awk -F' ' '{print \$1}'")
    #echo "${apacheDomain}"

    #change apache for wls, default
    weblogicDomain="wls${apacheDomain#apache}"
    #echo "${weblogicDomain}"

    eval $2="$weblogicDomain"
}

# read a property from a "properties" file
#
# arg[1] properties file
# arg[2] property name
#
function readProperty() {
    echo $(cat "$1" | egrep "^$2=" | sed "s/$2=//")
}

#
# arg[1] IP address
#
function getHostNameByIP() {
    echo $(host "$1" | cut  -d ' ' -f5 | cut -d '.' -f1)
}

#
# arg[1] host name
#
function getIPByDNSName() {
    echo $(ping "$1" -c 1 | grep "PING" | cut -d "(" -f2 | cut -d ")" -f1 )
}

function addCernDomainToString() {
    lowerCaseString="$(tr [A-Z] [a-z] <<< "$1")"
    echo ${lowerCaseString%.cern.ch}.cern.ch
}