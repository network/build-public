#!/bin/bash

# Script waiting for a DNS change.
# Arguments:
#   - DNS name to resolve
#   - Expected target Weblogic domain
#
# Example:
#   dnswait.sh csdb.cern.ch wls_csdb03_prod
#
# @author marwan.khelif@cern.ch

if [ "$#" -ne "2" ]; then
  echo "Usage: <dns name> <wl domain>"
  exit 1
fi

dnsAliasName="$1"
targetDomain="$2"

DIR=$(cd "$(dirname "$0")" && pwd -P)
source "${DIR}/wlutils.sh"

# Extract Apache server from domain
getApacheServer "${targetDomain}" targetApacheAlias
targetApacheHost=$(getHostNameByIP "$(getIPByDNSName "$targetApacheAlias")")

hostWhereTheAppIsRunning=""
while [ "${hostWhereTheAppIsRunning}" != "${targetApacheHost}" ]; do
  hostWhereTheAppIsRunning=$(getHostNameByIP "$(getIPByDNSName "$dnsAliasName")")
  echo "... ${dnsAliasName} -> ${DNS_ALIAS} ... waiting"

  sleep 20
done

# Wait 5 minutes to enable local cache to get refreshed
echo "DNS updated ... sleep for 5 minutes to enable local cache to get refreshed ... waiting"
sleep 60
echo "... 4 minutes to go ... waiting"
sleep 60
echo "... 3 minutes to go ... waiting"
sleep 60
echo "... 2 minutes to go ... waiting"
sleep 60
echo "... 60 seconds to go ... waiting"
sleep 30
echo "... 30 seconds to go ... waiting"
sleep 30

