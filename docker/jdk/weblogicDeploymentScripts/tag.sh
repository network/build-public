#!/bin/bash

# Script for tagging the application code.
#
# @author mkhelif & rgarcia

if [ "$#" -ne "2" ]; then
  echo "Usage: <application> <version>"
  exit 1
fi

set -e

application=$(echo "$1" | tr [A-Z] [a-z])
version="$2"

# Setup SSH key
eval $(ssh-agent -s)
ssh-add <(echo "$CI_SSH_KEY")

# Git settings
git remote set-url origin ssh://git@gitlab.cern.ch:7999/network/"${application}".git
git config user.name "GitlabCI"
git config user.email "csdb-admin@cern.ch"

# Tag version
git tag -a "${version}" -m "\"$(echo "$application" | tr [a-z] [A-Z]) version ${version}\""
git push origin "${version}"

# Stop SSH agent
ssh-agent -k
