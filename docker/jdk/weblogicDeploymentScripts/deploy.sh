#!/bin/bash

# Script for deploying Web application to server.
# Arguments:
#   - Application name (csdb, landb, guestwifi)
#   - Path to the EAR
#   - Weblogic domain to deploy to
#
# Example:
#   deploy.sh csdb csdbweb/dist/csdb-1625.ear wls_csdb03_prod
#
# @author marwan.khelif@cern.ch

set -e
if [ "$#" -ne "3" ]; then
  echo "Usage: <application> <archive> <wldomain>"
  exit 1
fi
application="$1"
archive="$2"
filename=$(basename "${archive}")
weblogicDomain="$3"

DIR=$(cd "$(dirname "$0")" && pwd -P)
source "${DIR}/wlutils.sh"


echo "Deploy ${application} to ${weblogicDomain} domain"

# Retrieve Admin server from Weblogic domain
getAdminServer "${weblogicDomain}" adminServer

# Send artifact
echo scp "${archive}" "landb@${adminServer}:~/${application}/apps/${filename}"
scp "${archive}" "landb@${adminServer}:~/${application}/apps/${filename}"


# Deploy artifact
echo ssh "landb@${adminServer}" "cd ~/${application} && ./deploy.sh apps/${filename}"
ssh "landb@${adminServer}" "cd ~/${application} && ./deploy.sh apps/${filename}"
