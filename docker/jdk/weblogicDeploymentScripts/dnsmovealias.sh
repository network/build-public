#!/bin/bash

# Script for moving a DNS alias to a different interface.
# Arguments:
#   - DNS alias to move
#   - Current Weblogic domain target of the alias
#   - New Weblogic domain target of the alias
#
# Example:
#   dnsmovealias.sh csdb.cern.ch wls_csdb03_prod wls_csdb13_prod
#
# @author marwan.khelif@cern.ch

if [ "$#" -ne "3" ]; then
  echo "Usage: <dns name> <source> <target>"
  exit 1
fi

DIR=$(cd "$(dirname "$0")" && pwd -P)
source "${DIR}/wlutils.sh"

dnsAliasName=$(addCernDomainToString "$1")
sourceDomain="$2"
destinationDomain="$3"


# Extract Apache server from domains
getApacheServer "${sourceDomain}" sourceApacheAlias
getApacheServer "${destinationDomain}" targetApacheAlias

# ping the get the ip of the apache server where our application is running
sourceApacheHost=$(addCernDomainToString "$(getHostNameByIP "$(getIPByDNSName "$sourceApacheAlias")")")
targetApacheHost=$(addCernDomainToString "$(getHostNameByIP "$(getIPByDNSName "$targetApacheAlias")")")


echo "Move alias ${dnsAliasName}: ${sourceApacheHost} -> ${targetApacheHost}"
echo java -cp "/opt/network/soap-client.jar" cern.network.build.soap.client.MoveAlias "${dnsAliasName}" "${sourceApacheHost}" "${targetApacheHost}"
java -cp "/opt/network/soap-client.jar" cern.network.build.soap.client.MoveAlias "${dnsAliasName}" "${sourceApacheHost}" "${targetApacheHost}"