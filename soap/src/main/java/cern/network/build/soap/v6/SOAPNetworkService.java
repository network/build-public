/**
 * SOAPNetworkService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package cern.network.build.soap.v6;

public interface SOAPNetworkService extends javax.xml.rpc.Service {
    String getNetworkServicePortAddress();

    NetworkServiceInterface getNetworkServicePort() throws javax.xml.rpc.ServiceException;

    NetworkServiceInterface getNetworkServicePort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
