package cern.network.build.soap.client;

import cern.network.build.soap.v6.Auth;
import cern.network.build.soap.v6.NetworkServiceInterface;
import cern.network.build.soap.v6.SOAPNetworkServiceLocator;

/**
 * SOAP script used to move an dns delegate alias from one delegate domain to another.
 * Usage:
 *   java -cp "<path to jar>" cern.network.build.soap.client.MoveDnsDelegateAlias <DNS alias> <sourceDelegateDomain> <targetDelegateDomain>
 *
 * @author rgarcia
 */
public class MoveDnsDelegateAlias {

    public static void main(final String[] args) throws Exception {
        if (args.length != 3) {
            System.out.println("Usage: java -cp \"<path to jar>\" cern.network.build.soap.client.MoveDnsDelegateAlias <DNS alias> <sourceDelegateDomain> <targetDelegateDomain>");
            System.exit(1);
            return;
        }

        final NetworkServiceInterface soap = new SOAPNetworkServiceLocator().getNetworkServicePort();

        final String alias = args[0];
        final String sourceDelegateDomain = args[1];
        final String targetDelegateDomain = args[2];

        final Auth token = new Auth(soap.getAuthToken(
                System.getenv("BUILD_USERNAME"),
                System.getenv("BUILD_PASSWORD"),
                "NICE"
        ));

        try {
            soap.dnsDelegatedAliasRemove(token, sourceDelegateDomain, "EXTERNAL", alias);
            soap.dnsDelegatedAliasRemove(token, sourceDelegateDomain, "INTERNAL", alias);

            soap.dnsDelegatedAliasAdd(token, targetDelegateDomain, "INTERNAL", alias);
            soap.dnsDelegatedAliasAdd(token, targetDelegateDomain, "EXTERNAL", alias);
        } catch (org.apache.axis.AxisFault axisFaultException){
            if (axisFaultException.getFaultCode().toString().equals("{http://schemas.xmlsoap.org/soap/envelope/}Server.userException")) {
                System.err.println("**************************************************************************************");
                System.err.println("You probably are having a SOAP error, check if you domains and alias have \".cern.ch\" and that the alias is in the source domain");
                System.err.println("**************************************************************************************");
            }
            throw axisFaultException;
        }

    }
}


/*
If the error is [1] is likely that you are getting a SOAP error and not a SOAP client error.
Check it you domains and alias have ".cern.ch" and that the alias is in the source domain

[1]
Exception in thread "main" AxisFault
 faultCode: {http://schemas.xmlsoap.org/soap/envelope/}Server.userException
 faultSubcode:
 faultString: org.w3c.dom.DOMException: WRONG_DOCUMENT_ERR: A node is used in a different document than the one that created it.
 faultActor:
 faultNode:
 faultDetail:
        {http://xml.apache.org/axis/}stackTrace:org.w3c.dom.DOMException: WRONG_DOCUMENT_ERR: A node is used in a different document than the one that created it.
 */

