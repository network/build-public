/**
 * DeviceInput.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package cern.network.build.soap.v6;

public class DeviceInput  implements java.io.Serializable {
    private java.lang.String deviceName;

    private cern.network.build.soap.v6.Location location;

    private java.lang.String zone;

    private java.lang.String manufacturer;

    private java.lang.String model;

    private java.lang.String description;

    private java.lang.String tag;

    private java.lang.String serialNumber;

    private cern.network.build.soap.v6.OperatingSystem operatingSystem;

    private java.lang.String inventoryNumber;

    private cern.network.build.soap.v6.PersonInput landbManagerPerson;

    private cern.network.build.soap.v6.PersonInput responsiblePerson;

    private cern.network.build.soap.v6.PersonInput userPerson;

    private java.lang.Boolean HCPResponse;

    private java.lang.Boolean IPv6Ready;

    private java.lang.Boolean managerLocked;

    public DeviceInput() {
    }

    public DeviceInput(
           java.lang.String deviceName,
           cern.network.build.soap.v6.Location location,
           java.lang.String zone,
           java.lang.String manufacturer,
           java.lang.String model,
           java.lang.String description,
           java.lang.String tag,
           java.lang.String serialNumber,
           cern.network.build.soap.v6.OperatingSystem operatingSystem,
           java.lang.String inventoryNumber,
           cern.network.build.soap.v6.PersonInput landbManagerPerson,
           cern.network.build.soap.v6.PersonInput responsiblePerson,
           cern.network.build.soap.v6.PersonInput userPerson,
           java.lang.Boolean HCPResponse,
           java.lang.Boolean IPv6Ready,
           java.lang.Boolean managerLocked) {
           this.deviceName = deviceName;
           this.location = location;
           this.zone = zone;
           this.manufacturer = manufacturer;
           this.model = model;
           this.description = description;
           this.tag = tag;
           this.serialNumber = serialNumber;
           this.operatingSystem = operatingSystem;
           this.inventoryNumber = inventoryNumber;
           this.landbManagerPerson = landbManagerPerson;
           this.responsiblePerson = responsiblePerson;
           this.userPerson = userPerson;
           this.HCPResponse = HCPResponse;
           this.IPv6Ready = IPv6Ready;
           this.managerLocked = managerLocked;
    }


    /**
     * Gets the deviceName value for this DeviceInput.
     *
     * @return deviceName
     */
    public java.lang.String getDeviceName() {
        return deviceName;
    }


    /**
     * Sets the deviceName value for this DeviceInput.
     *
     * @param deviceName
     */
    public void setDeviceName(java.lang.String deviceName) {
        this.deviceName = deviceName;
    }


    /**
     * Gets the location value for this DeviceInput.
     *
     * @return location
     */
    public cern.network.build.soap.v6.Location getLocation() {
        return location;
    }


    /**
     * Sets the location value for this DeviceInput.
     *
     * @param location
     */
    public void setLocation(cern.network.build.soap.v6.Location location) {
        this.location = location;
    }


    /**
     * Gets the zone value for this DeviceInput.
     *
     * @return zone
     */
    public java.lang.String getZone() {
        return zone;
    }


    /**
     * Sets the zone value for this DeviceInput.
     *
     * @param zone
     */
    public void setZone(java.lang.String zone) {
        this.zone = zone;
    }


    /**
     * Gets the manufacturer value for this DeviceInput.
     *
     * @return manufacturer
     */
    public java.lang.String getManufacturer() {
        return manufacturer;
    }


    /**
     * Sets the manufacturer value for this DeviceInput.
     *
     * @param manufacturer
     */
    public void setManufacturer(java.lang.String manufacturer) {
        this.manufacturer = manufacturer;
    }


    /**
     * Gets the model value for this DeviceInput.
     *
     * @return model
     */
    public java.lang.String getModel() {
        return model;
    }


    /**
     * Sets the model value for this DeviceInput.
     *
     * @param model
     */
    public void setModel(java.lang.String model) {
        this.model = model;
    }


    /**
     * Gets the description value for this DeviceInput.
     *
     * @return description
     */
    public java.lang.String getDescription() {
        return description;
    }


    /**
     * Sets the description value for this DeviceInput.
     *
     * @param description
     */
    public void setDescription(java.lang.String description) {
        this.description = description;
    }


    /**
     * Gets the tag value for this DeviceInput.
     *
     * @return tag
     */
    public java.lang.String getTag() {
        return tag;
    }


    /**
     * Sets the tag value for this DeviceInput.
     *
     * @param tag
     */
    public void setTag(java.lang.String tag) {
        this.tag = tag;
    }


    /**
     * Gets the serialNumber value for this DeviceInput.
     *
     * @return serialNumber
     */
    public java.lang.String getSerialNumber() {
        return serialNumber;
    }


    /**
     * Sets the serialNumber value for this DeviceInput.
     *
     * @param serialNumber
     */
    public void setSerialNumber(java.lang.String serialNumber) {
        this.serialNumber = serialNumber;
    }


    /**
     * Gets the operatingSystem value for this DeviceInput.
     *
     * @return operatingSystem
     */
    public cern.network.build.soap.v6.OperatingSystem getOperatingSystem() {
        return operatingSystem;
    }


    /**
     * Sets the operatingSystem value for this DeviceInput.
     *
     * @param operatingSystem
     */
    public void setOperatingSystem(cern.network.build.soap.v6.OperatingSystem operatingSystem) {
        this.operatingSystem = operatingSystem;
    }


    /**
     * Gets the inventoryNumber value for this DeviceInput.
     *
     * @return inventoryNumber
     */
    public java.lang.String getInventoryNumber() {
        return inventoryNumber;
    }


    /**
     * Sets the inventoryNumber value for this DeviceInput.
     *
     * @param inventoryNumber
     */
    public void setInventoryNumber(java.lang.String inventoryNumber) {
        this.inventoryNumber = inventoryNumber;
    }


    /**
     * Gets the landbManagerPerson value for this DeviceInput.
     *
     * @return landbManagerPerson
     */
    public cern.network.build.soap.v6.PersonInput getLandbManagerPerson() {
        return landbManagerPerson;
    }


    /**
     * Sets the landbManagerPerson value for this DeviceInput.
     *
     * @param landbManagerPerson
     */
    public void setLandbManagerPerson(cern.network.build.soap.v6.PersonInput landbManagerPerson) {
        this.landbManagerPerson = landbManagerPerson;
    }


    /**
     * Gets the responsiblePerson value for this DeviceInput.
     *
     * @return responsiblePerson
     */
    public cern.network.build.soap.v6.PersonInput getResponsiblePerson() {
        return responsiblePerson;
    }


    /**
     * Sets the responsiblePerson value for this DeviceInput.
     *
     * @param responsiblePerson
     */
    public void setResponsiblePerson(cern.network.build.soap.v6.PersonInput responsiblePerson) {
        this.responsiblePerson = responsiblePerson;
    }


    /**
     * Gets the userPerson value for this DeviceInput.
     *
     * @return userPerson
     */
    public cern.network.build.soap.v6.PersonInput getUserPerson() {
        return userPerson;
    }


    /**
     * Sets the userPerson value for this DeviceInput.
     *
     * @param userPerson
     */
    public void setUserPerson(cern.network.build.soap.v6.PersonInput userPerson) {
        this.userPerson = userPerson;
    }


    /**
     * Gets the HCPResponse value for this DeviceInput.
     *
     * @return HCPResponse
     */
    public java.lang.Boolean getHCPResponse() {
        return HCPResponse;
    }


    /**
     * Sets the HCPResponse value for this DeviceInput.
     *
     * @param HCPResponse
     */
    public void setHCPResponse(java.lang.Boolean HCPResponse) {
        this.HCPResponse = HCPResponse;
    }


    /**
     * Gets the IPv6Ready value for this DeviceInput.
     *
     * @return IPv6Ready
     */
    public java.lang.Boolean getIPv6Ready() {
        return IPv6Ready;
    }


    /**
     * Sets the IPv6Ready value for this DeviceInput.
     *
     * @param IPv6Ready
     */
    public void setIPv6Ready(java.lang.Boolean IPv6Ready) {
        this.IPv6Ready = IPv6Ready;
    }


    /**
     * Gets the managerLocked value for this DeviceInput.
     *
     * @return managerLocked
     */
    public java.lang.Boolean getManagerLocked() {
        return managerLocked;
    }


    /**
     * Sets the managerLocked value for this DeviceInput.
     *
     * @param managerLocked
     */
    public void setManagerLocked(java.lang.Boolean managerLocked) {
        this.managerLocked = managerLocked;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof DeviceInput)) return false;
        DeviceInput other = (DeviceInput) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true &&
            ((this.deviceName==null && other.getDeviceName()==null) ||
             (this.deviceName!=null &&
              this.deviceName.equals(other.getDeviceName()))) &&
            ((this.location==null && other.getLocation()==null) ||
             (this.location!=null &&
              this.location.equals(other.getLocation()))) &&
            ((this.zone==null && other.getZone()==null) ||
             (this.zone!=null &&
              this.zone.equals(other.getZone()))) &&
            ((this.manufacturer==null && other.getManufacturer()==null) ||
             (this.manufacturer!=null &&
              this.manufacturer.equals(other.getManufacturer()))) &&
            ((this.model==null && other.getModel()==null) ||
             (this.model!=null &&
              this.model.equals(other.getModel()))) &&
            ((this.description==null && other.getDescription()==null) ||
             (this.description!=null &&
              this.description.equals(other.getDescription()))) &&
            ((this.tag==null && other.getTag()==null) ||
             (this.tag!=null &&
              this.tag.equals(other.getTag()))) &&
            ((this.serialNumber==null && other.getSerialNumber()==null) ||
             (this.serialNumber!=null &&
              this.serialNumber.equals(other.getSerialNumber()))) &&
            ((this.operatingSystem==null && other.getOperatingSystem()==null) ||
             (this.operatingSystem!=null &&
              this.operatingSystem.equals(other.getOperatingSystem()))) &&
            ((this.inventoryNumber==null && other.getInventoryNumber()==null) ||
             (this.inventoryNumber!=null &&
              this.inventoryNumber.equals(other.getInventoryNumber()))) &&
            ((this.landbManagerPerson==null && other.getLandbManagerPerson()==null) ||
             (this.landbManagerPerson!=null &&
              this.landbManagerPerson.equals(other.getLandbManagerPerson()))) &&
            ((this.responsiblePerson==null && other.getResponsiblePerson()==null) ||
             (this.responsiblePerson!=null &&
              this.responsiblePerson.equals(other.getResponsiblePerson()))) &&
            ((this.userPerson==null && other.getUserPerson()==null) ||
             (this.userPerson!=null &&
              this.userPerson.equals(other.getUserPerson()))) &&
            ((this.HCPResponse==null && other.getHCPResponse()==null) ||
             (this.HCPResponse!=null &&
              this.HCPResponse.equals(other.getHCPResponse()))) &&
            ((this.IPv6Ready==null && other.getIPv6Ready()==null) ||
             (this.IPv6Ready!=null &&
              this.IPv6Ready.equals(other.getIPv6Ready()))) &&
            ((this.managerLocked==null && other.getManagerLocked()==null) ||
             (this.managerLocked!=null &&
              this.managerLocked.equals(other.getManagerLocked())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDeviceName() != null) {
            _hashCode += getDeviceName().hashCode();
        }
        if (getLocation() != null) {
            _hashCode += getLocation().hashCode();
        }
        if (getZone() != null) {
            _hashCode += getZone().hashCode();
        }
        if (getManufacturer() != null) {
            _hashCode += getManufacturer().hashCode();
        }
        if (getModel() != null) {
            _hashCode += getModel().hashCode();
        }
        if (getDescription() != null) {
            _hashCode += getDescription().hashCode();
        }
        if (getTag() != null) {
            _hashCode += getTag().hashCode();
        }
        if (getSerialNumber() != null) {
            _hashCode += getSerialNumber().hashCode();
        }
        if (getOperatingSystem() != null) {
            _hashCode += getOperatingSystem().hashCode();
        }
        if (getInventoryNumber() != null) {
            _hashCode += getInventoryNumber().hashCode();
        }
        if (getLandbManagerPerson() != null) {
            _hashCode += getLandbManagerPerson().hashCode();
        }
        if (getResponsiblePerson() != null) {
            _hashCode += getResponsiblePerson().hashCode();
        }
        if (getUserPerson() != null) {
            _hashCode += getUserPerson().hashCode();
        }
        if (getHCPResponse() != null) {
            _hashCode += getHCPResponse().hashCode();
        }
        if (getIPv6Ready() != null) {
            _hashCode += getIPv6Ready().hashCode();
        }
        if (getManagerLocked() != null) {
            _hashCode += getManagerLocked().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(DeviceInput.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:cern.network.build.soap.v6", "DeviceInput"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("deviceName");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:cern.network.build.soap.v6", "DeviceName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("location");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:cern.network.build.soap.v6", "Location"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:cern.network.build.soap.v6", "Location"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("zone");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:cern.network.build.soap.v6", "Zone"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("manufacturer");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:cern.network.build.soap.v6", "Manufacturer"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("model");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:cern.network.build.soap.v6", "Model"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("description");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:cern.network.build.soap.v6", "Description"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tag");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:cern.network.build.soap.v6", "Tag"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("serialNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:cern.network.build.soap.v6", "SerialNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("operatingSystem");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:cern.network.build.soap.v6", "OperatingSystem"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:cern.network.build.soap.v6", "OperatingSystem"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("inventoryNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:cern.network.build.soap.v6", "InventoryNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("landbManagerPerson");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:cern.network.build.soap.v6", "LandbManagerPerson"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:cern.network.build.soap.v6", "PersonInput"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("responsiblePerson");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:cern.network.build.soap.v6", "ResponsiblePerson"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:cern.network.build.soap.v6", "PersonInput"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("userPerson");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:cern.network.build.soap.v6", "UserPerson"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:cern.network.build.soap.v6", "PersonInput"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("HCPResponse");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:cern.network.build.soap.v6", "HCPResponse"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("IPv6Ready");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:cern.network.build.soap.v6", "IPv6Ready"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("managerLocked");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:cern.network.build.soap.v6", "ManagerLocked"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType,
           java.lang.Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType,
           java.lang.Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
