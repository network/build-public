/**
 * NetworkServiceInterface.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package cern.network.build.soap.v6;

public interface NetworkServiceInterface extends java.rmi.Remote {
    String getAuthToken(String login, String password, String type) throws java.rmi.RemoteException;
    String[] searchDevice(Auth auth, DeviceSearch deviceSearch) throws java.rmi.RemoteException;
    DeviceBasicInfo getDeviceBasicInfo(String deviceName) throws java.rmi.RemoteException;
    DeviceInfo getDeviceInfo(Auth auth, String deviceName) throws java.rmi.RemoteException;
    DeviceInfo[] getDeviceInfoArray(Auth auth, String[] deviceNameList) throws java.rmi.RemoteException;
    DeviceInfo getDeviceInfoFromNameMAC(String deviceName, String MAC) throws java.rmi.RemoteException;
    DeviceInfo getMyDeviceInfo() throws java.rmi.RemoteException;
    String[] getLastChangedDevices(Auth auth, int minutes) throws java.rmi.RemoteException;
    boolean bulkInsert(Auth auth, DeviceInput device, InterfaceCard[] cards, BulkInterface[] interfaces) throws java.rmi.RemoteException;
    boolean bulkInsertAuto(DeviceInput device, InterfaceCard[] cards, BulkInterfaceAuto[] interfaces) throws java.rmi.RemoteException;
    boolean bulkRemove(Auth auth, String deviceName) throws java.rmi.RemoteException;
    boolean deviceInsert(Auth auth, DeviceInput device) throws java.rmi.RemoteException;
    boolean deviceAddCard(Auth auth, String deviceName, InterfaceCard interfaceCard) throws java.rmi.RemoteException;
    boolean deviceAddBulkInterface(Auth auth, String deviceName, BulkInterface bulkInterface) throws java.rmi.RemoteException;
    boolean deviceRemove(Auth auth, String deviceName) throws java.rmi.RemoteException;
    boolean deviceRemoveCard(Auth auth, String deviceName, String hardwareAddress) throws java.rmi.RemoteException;
    boolean deviceRemoveBulkInterface(Auth auth, String deviceName, String interfaceName) throws java.rmi.RemoteException;
    boolean deviceMoveBulkInterface(Auth auth, String deviceName, String interfaceName, BulkInterface bulkInterface, BulkMoveOptions bulkMoveOptions) throws java.rmi.RemoteException;
    boolean deviceUpdate(Auth auth, String deviceName, DeviceInput deviceInput) throws java.rmi.RemoteException;
    boolean deviceGlobalRename(Auth auth, String deviceName, String newDeviceName) throws java.rmi.RemoteException;
    boolean setHCPResponse(Auth auth, String[] deviceList, boolean HCPFlag) throws java.rmi.RemoteException;
    boolean deviceUpdateIPv6Ready(Auth auth, String deviceName, boolean IPv6Ready) throws java.rmi.RemoteException;
    boolean deviceUpdateManagerLock(Auth auth, String deviceName, boolean managerLocked) throws java.rmi.RemoteException;
    boolean deviceSetBOOTPInfo(Auth auth, String device, String server, String imagePath) throws java.rmi.RemoteException;
    boolean deviceRemoveBOOTPInfo(Auth auth, String device) throws java.rmi.RemoteException;
    BOOTPInfo getBOOTPInfo(Auth auth, String device) throws java.rmi.RemoteException;
    BulkInterface getBulkInterfaceInfo(Auth auth, String interfaceName) throws java.rmi.RemoteException;
    boolean setInsertAddress(Auth auth, String set, String address) throws java.rmi.RemoteException;
    boolean setInsertService(Auth auth, String set, String service) throws java.rmi.RemoteException;
    boolean setDeleteAddress(Auth auth, String set, String address) throws java.rmi.RemoteException;
    boolean setDeleteService(Auth auth, String set, String service) throws java.rmi.RemoteException;
    SetInfo getSetInfo(Auth auth, String setName) throws java.rmi.RemoteException;
    String getSetNameFromID(Auth auth, long setID) throws java.rmi.RemoteException;
    String[] getSetAllInterfaces(Auth auth, String setName) throws java.rmi.RemoteException;
    String[] getSetInterfacesTrusting(Auth auth, String setName) throws java.rmi.RemoteException;
    InetInfo[] getHCPInfoArray(Auth auth, String[] hosts) throws java.rmi.RemoteException;
    String[] getDevicesFromService(Auth auth, String service) throws java.rmi.RemoteException;
    String[] getSwitchesFromService(Auth auth, String service) throws java.rmi.RemoteException;
    SwitchPort[] getSwitchInfo(Auth auth, String switchName) throws java.rmi.RemoteException;
    Connection[] getConnectionsFromDevice(Auth auth, String deviceName) throws java.rmi.RemoteException;
    OutletLocation getOutletLocationFromSwitchPort(Auth auth, String switchName, String portName) throws java.rmi.RemoteException;
    ObservedSwitchConnection[] getCurrentConnection(String ip, String[] hardwareAddressList) throws java.rmi.RemoteException;
    ObservedSwitchConnection[] getMyCurrentConnection(String[] hardwareAddressList) throws java.rmi.RemoteException;
    boolean enableFanOutFromSwitchPort(Auth auth, String switchName, String portName) throws java.rmi.RemoteException;
    boolean bindUnbindInterface(Auth auth, String interfaceName, String hardwareAddress) throws java.rmi.RemoteException;
    boolean interfaceAddAlias(Auth auth, String interfaceName, String alias) throws java.rmi.RemoteException;
    boolean interfaceRemoveAlias(Auth auth, String interfaceName, String alias) throws java.rmi.RemoteException;
    boolean interfaceMoveAlias(Auth auth, String interfaceName, String alias, String newInterfaceName) throws java.rmi.RemoteException;
    boolean interfaceRename(Auth auth, String interfaceName, String newInterfaceName) throws java.rmi.RemoteException;
    boolean interfaceMove(Auth auth, String interfaceName, String newDeviceName) throws java.rmi.RemoteException;
    String[] searchSet(Auth auth, String setPattern) throws java.rmi.RemoteException;
    boolean setInsert(Auth auth, SetInput set) throws java.rmi.RemoteException;
    boolean setRemove(Auth auth, String setName) throws java.rmi.RemoteException;
    boolean setSwitchPortTypeStatus(Auth auth, String switchName, String portName, SwitchPortTypeStatus switchPortTypeStatus) throws java.rmi.RemoteException;
    boolean setSwitchPortService(Auth auth, String switchName, String portName, String service) throws java.rmi.RemoteException;
    SwitchPortTypeStatus getSwitchPortTypeStatus(Auth auth, String switchName, String portName) throws java.rmi.RemoteException;
    NetNameTuple[] searchNetNameTable(Auth auth, String netName) throws java.rmi.RemoteException;
    boolean deviceAddLogicalInterface(Auth auth, String deviceName, LogicalInterfaceInput logicalInterface) throws java.rmi.RemoteException;
    boolean deviceRemoveLogicalInterface(Auth auth, String deviceName, String interfaceName) throws java.rmi.RemoteException;
    boolean interfaceUpdateDescription(Auth auth, String interfaceName, String description) throws java.rmi.RemoteException;
    boolean serviceUpdateDescription(Auth auth, String serviceName, String description) throws java.rmi.RemoteException;
    ServiceInfo getServiceInfo(Auth auth, String serviceName) throws java.rmi.RemoteException;
    boolean vmCreate(Auth auth, DeviceInput VMDevice, VMCreateOptions VMCreateOptions) throws java.rmi.RemoteException;
    boolean vmMigrate(Auth auth, String VMName, String newParent) throws java.rmi.RemoteException;
    boolean vmUpdate(Auth auth, String deviceName, DeviceInput deviceInput) throws java.rmi.RemoteException;
    boolean vmDestroy(Auth auth, String VMName) throws java.rmi.RemoteException;
    VMClusterInfo vmClusterGetInfo(Auth auth, String VMClusterName) throws java.rmi.RemoteException;
    String[] vmClusterGetDevices(Auth auth, String VMClusterName) throws java.rmi.RemoteException;
    VMInfo vmGetInfo(Auth auth, String VMName) throws java.rmi.RemoteException;
    String[] vmGetClusterMembership(Auth auth, String deviceName) throws java.rmi.RemoteException;
    String[] vmSearchCluster(Auth auth, VMClusterSearch VMClusterSearch) throws java.rmi.RemoteException;
    boolean vmAddInterface(Auth auth, String VMName, String interfaceName, String VMClusterName, VMInterfaceOptions VMInterfaceOptions) throws java.rmi.RemoteException;
    boolean vmRemoveInterface(Auth auth, String VMName, String interfaceName) throws java.rmi.RemoteException;
    boolean vmMoveInterface(Auth auth, String VMName, String interfaceName, String VMClusterName, VMInterfaceOptions VMInterfaceOptions) throws java.rmi.RemoteException;
    String vmAddCard(Auth auth, String VMName, InterfaceCard interfaceCard) throws java.rmi.RemoteException;
    boolean vmRemoveCard(Auth auth, String VMName, String hardwareAddress) throws java.rmi.RemoteException;
    boolean dnsZoneUpdate(Auth auth, String zone, DnsZoneOptions dnsZoneOptions) throws java.rmi.RemoteException;
    DNSDelegatedEntry[] dnsDelegatedSearch(Auth auth, String search) throws java.rmi.RemoteException;
    DNSDelegatedEntry dnsDelegatedGetByNameView(Auth auth, String domain, String view) throws java.rmi.RemoteException;
    boolean dnsDelegatedAdd(Auth auth, DNSDelegatedInput DNSDelegatedInput) throws java.rmi.RemoteException;
    DNSDelegatedKey[] dnsDelegatedListKeys(Auth auth) throws java.rmi.RemoteException;
    boolean dnsDelegatedRemove(Auth auth, String domain, String view) throws java.rmi.RemoteException;
    boolean dnsDelegatedAliasAdd(Auth auth, String domain, String view, String alias) throws java.rmi.RemoteException;
    boolean dnsDelegatedAliasRemove(Auth auth, String domain, String view, String alias) throws java.rmi.RemoteException;
}
